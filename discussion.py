class Sample_Class():

	def __init__(self, year):
		self.year = year

	def show_year(self):
		print(f'The year is: {self.year}')

sample_obj = Sample_Class(2023)

print(sample_obj.year)
sample_obj.show_year()

# [Section] Fundamentals of OOP
# Four pillars of OOP
# Encapsulation
# Inheritance
# Polymorphism
# Abstraction

# First Pillar: Encapsulation
# Encapsulation is a mechanism of wrapping the attributes and code acting on the methods together as a single

class Person():
	
	def __init__(self, name, age):
		self._name = name
		self._age = age

	# Methods 
	# getter of _name attribute
	def get_name(self):
		print(f'Name of person: {self._name}')

	# setter of _name attribute
	def set_name(self, name):
		self._name = name

	# getter of _age attribute
	def get_age(self):
		print(f'Age of ferson: {self._age}')

	#setter of _age attribute
	def set_age(self, age):
		self._age = age


# new instance
person_one = Person("James", 245)

person_one.get_name()

person_one.set_name("Kat")

person_one.get_name()

person_one.get_age()

person_one.set_age(3)
person_one.get_age()

# Second Pillar: Inheritance
# The inherittance of the characteristic or attributes of a parent class to a child class that are derived from it

# Syntax: class child_class_name(parent_class_name):

class Employee(Person):
	def __init__(self, name, age, employee_id):
		# super() can be used to invoke immediate parent class constructor
		super().__init__(name, age)

		self._employee_id = employee_id

	# method for the _employee_id attrib
	def get_employee_id(self):
		print(f'The employee ID is {self._employee_id}')

	def set_employee_id(self, employee_id):
		self._employee_id = employee_id

employee_one = Employee("CongTV", 27, "0001")

employee_one.get_age()
employee_one.set_age(28)
employee_one.get_age()

employee_one.get_name()
employee_one.set_name("Lincoln")
employee_one.get_name()

employee_one.get_employee_id()
employee_one.set_employee_id("legend")
employee_one.get_employee_id()








