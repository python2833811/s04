from abc import ABC

class Animal(ABC):
    def eat(self):
        pass

    def make_sound(self):
        pass

class Cat(Animal):
    def __init__(self,name,breed,age):
        super().__init__()
        self._name = name
        self._breed = breed
        self._age = age

    def get_name(self):
        return self._name

    def get_breed(self):
        return self._breed
    
    def get_age(self):
        return self._age
    
    def set_name(self,name):
        self._name = name

    def set_breed(self, breed):
        self._breed = breed
    
    def set_age(self,age):
        self._age = age

    def eat(self, food):
        print(f"Cat has eaten {food}")
    
    def make_sound(self):
        print("Miyaoo Nyaaa!")
    
    def call(self):
        print(f"Here {self._name}")    

class Dog(Animal):
    def __init__(self,name,breed,age):
        super().__init__()
        self._name = name
        self._breed = breed
        self._age = age

    def get_name(self):
        return self._name

    def get_breed(self):
        return self._breed
    
    def get_age(self):
        return self._age
    
    def set_name(self,name):
        self._name = name

    def set_breed(self, breed):
        self._breed = breed
    
    def set_age(self,age):
        self._age = age

    def eat(self, food):
        print(f"Dog has eaten {food}")
    
    def make_sound(self):
        print("Bark! Woof!")
    
    def call(self):
        print(f"Here {self._name}")    

dog1 = Dog("Isis", "Dalmatian",15)
dog1.eat("Steak")
dog1.make_sound()
dog1.call()

cat = Cat("Puss", "Persian",4)
cat.eat("Tuna")
cat.make_sound()
cat.call()